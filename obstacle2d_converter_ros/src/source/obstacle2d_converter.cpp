#include "obstacle2d_converter_ros/obstacle2d_converter.h"


ObstacleAssociation::~ObstacleAssociation()
{
    if(obstacle3d_)
        delete obstacle3d_;
    if(obstacle2d_)
        delete obstacle2d_;

    return;
}






Obstacle2dConverterRos::Obstacle2dConverterRos(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, ros::this_node::getName());

    // Node handle
    nh_=new ros::NodeHandle();

    // info
    ROS_INFO("Starting %s",ros::this_node::getName().c_str());

    // read parameters
    readParameters();

    // end
    return;
}

Obstacle2dConverterRos::~Obstacle2dConverterRos()
{
    for(std::list<ObstacleAssociation*>::iterator it_list_obstacle=list_obstacle_.begin();
        it_list_obstacle!=list_obstacle_.end();
        ++it_list_obstacle)
    {
        delete (*it_list_obstacle);
    }
    list_obstacle_.clear();

    return;
}

void Obstacle2dConverterRos::readParameters()
{
    //
    ros::param::param<std::string>("~pose_topic_name", pose_topic_name_, "pose");
    ROS_INFO("pose_topic_name = %s",pose_topic_name_.c_str());
    //
    ros::param::param<std::string>("~obstacles_3d_topic_name", obstacles_3d_topic_name_, "obstacles3d");
    ROS_INFO("obstacles_3d_topic_name = %s",obstacles_3d_topic_name_.c_str());
    //
    ros::param::param<std::string>("~obstacles_2d_topic_name", obstacles_2d_topic_name_, "obstacles2d");
    ROS_INFO("obstacles_2d_topic_name = %s",obstacles_2d_topic_name_.c_str());

    // end
    return;
}

int Obstacle2dConverterRos::open()
{
    // Subscribers
    pose_sub_ = nh_->subscribe(pose_topic_name_, 1, &Obstacle2dConverterRos::poseCallback, this);
    obstacles_3d_sub_ = nh_->subscribe(obstacles_3d_topic_name_, 1, &Obstacle2dConverterRos::obstacles3dCallback, this);

    // Publishers
    obstacles_2d_pub_ = nh_->advertise<environment_msgs::Object2dArray>(obstacles_2d_topic_name_, 10);

    // end
    return 0;
}

int Obstacle2dConverterRos::run()
{
    ros::spin();

    return 0;
}

void Obstacle2dConverterRos::poseCallback(const geometry_msgs::PoseStamped::ConstPtr &msg)
{
    // Update plane3d

    // Horizontal plane
    Eigen::Vector3d normal(0, 0, 1);
    double distance=-msg->pose.position.z;

    // Set
    plane3d_.setPlane3d(normal, distance);


    // publish
    publish();

    // end
    return;
}

void Obstacle2dConverterRos::obstacles3dCallback(const visualization_msgs::MarkerArray::ConstPtr &msg)
{
    // check
    if(msg->markers.empty())
        return;

    //ROS_INFO("msg->markers.size()=%d",msg->markers.size());
    //ROS_INFO("list_obstacle_.size()=%d",list_obstacle_.size());

    // Update list of obstacles3d
    for(int i=0; i<msg->markers.size(); i++)
    {
        // find obstacle in list by id
        std::list<ObstacleAssociation*>::iterator it_list_obstacle;
        bool found=false;
        for(it_list_obstacle=list_obstacle_.begin();
            it_list_obstacle!=list_obstacle_.end();
            ++it_list_obstacle)
        {
            if((*it_list_obstacle)->id_ == msg->markers[i].id)
            {
                found=true;
                //ROS_INFO("Found id=%d",(*it_list_obstacle)->id_);
                break;
            }
        }


        // Obstacle
         ObstacleAssociation* obstacle;

        // create if it does not exist
        if(!found || it_list_obstacle==list_obstacle_.end())
        {
            //ROS_INFO("Id=%d not found. Creating new ObstacleAssociation", msg->markers[i].id);
            // ObstacleAssociation
            obstacle=new ObstacleAssociation();

            // id
            obstacle->id_=msg->markers[i].id;

            // obstacle3d by Type
            switch(msg->markers[i].type)
            {
                case visualization_msgs::Marker::CUBE:
                {
                    Eigen::Vector3d position_cube_wrt_world(msg->markers[i].pose.position.x, msg->markers[i].pose.position.y, msg->markers[i].pose.position.z);
                    Eigen::Vector4d attitude_cube_wrt_world(msg->markers[i].pose.orientation.w, msg->markers[i].pose.orientation.x, msg->markers[i].pose.orientation.y, msg->markers[i].pose.orientation.z);
                    Eigen::Vector3d size(msg->markers[i].scale.x, msg->markers[i].scale.y, msg->markers[i].scale.z);
                    Obstacle3d* obstacle3d=new Cuboid3d(position_cube_wrt_world, attitude_cube_wrt_world, size);
                    obstacle->obstacle3d_=obstacle3d;
                    break;
                }
                case visualization_msgs::Marker::CYLINDER:
                {
                    Eigen::Vector3d position_cylinder_wrt_world(msg->markers[i].pose.position.x, msg->markers[i].pose.position.y, msg->markers[i].pose.position.z);
                    Eigen::Vector4d attitude_cylinder_wrt_world(msg->markers[i].pose.orientation.w, msg->markers[i].pose.orientation.x, msg->markers[i].pose.orientation.y, msg->markers[i].pose.orientation.z);
                    Eigen::Vector2d radii(0.5*msg->markers[i].scale.x, 0.5*msg->markers[i].scale.y);
                    double height=msg->markers[i].scale.z;

                    Obstacle3d* obstacle3d=new Cylinder3d(position_cylinder_wrt_world, attitude_cylinder_wrt_world, radii, height);
                    obstacle->obstacle3d_=obstacle3d;
                    break;
                }
                case visualization_msgs::Marker::SPHERE:
                {
                    // TODO
                }
                default:
                    break;
            }

            // push
            list_obstacle_.push_back(obstacle);
        }
        // update if it exists
        else
        {
            //ROS_INFO("I have obstacle with id=%d",(*it_list_obstacle)->id_);
            // Obstacle
            obstacle = (*it_list_obstacle);

            // obstacle3d by Type
            switch(msg->markers[i].type)
            {
                case visualization_msgs::Marker::CUBE:
                {
                    Cuboid3d* cuboid3d=static_cast<Cuboid3d*>((*it_list_obstacle)->obstacle3d_);

                    Eigen::Vector3d position_cube_wrt_world(msg->markers[i].pose.position.x, msg->markers[i].pose.position.y, msg->markers[i].pose.position.z);
                    Eigen::Vector4d attitude_cube_wrt_world(msg->markers[i].pose.orientation.w, msg->markers[i].pose.orientation.x, msg->markers[i].pose.orientation.y, msg->markers[i].pose.orientation.z);
                    Eigen::Vector3d size(msg->markers[i].scale.x, msg->markers[i].scale.y, msg->markers[i].scale.z);

                    cuboid3d->setPoseCenterWrtWorld(position_cube_wrt_world, attitude_cube_wrt_world);
                    cuboid3d->setSize(size);

                    break;
                }
                case visualization_msgs::Marker::CYLINDER:
                {
                    Cylinder3d* cylinder3d=static_cast<Cylinder3d*>((*it_list_obstacle)->obstacle3d_);

                    Eigen::Vector3d position_cylinder_wrt_world(msg->markers[i].pose.position.x, msg->markers[i].pose.position.y, msg->markers[i].pose.position.z);
                    Eigen::Vector4d attitude_cylinder_wrt_world(msg->markers[i].pose.orientation.w, msg->markers[i].pose.orientation.x, msg->markers[i].pose.orientation.y, msg->markers[i].pose.orientation.z);
                    Eigen::Vector2d radii(0.5*msg->markers[i].scale.x, 0.5*msg->markers[i].scale.y);
                    double height=msg->markers[i].scale.z;

                    cylinder3d->setPoseCenterWrtWorld(position_cylinder_wrt_world, attitude_cylinder_wrt_world);
                    cylinder3d->setRadii(radii);
                    cylinder3d->setHeight(height);

                    break;
                }
                case visualization_msgs::Marker::SPHERE:
                {
                    // TODO
                }
                default:
                    break;
            }
        }

        // Obstacle action
        switch(msg->markers[i].action)
        {
            case visualization_msgs::Marker::ADD:
            //case visualization_msgs::Marker::MODIFY: // Already included, same value than ADD
                obstacle->obstacle3d_->setObstacleAction(ObstacleAction::update_obstacle);
                break;
            case visualization_msgs::Marker::DELETE:
            //case visualization_msgs::Marker::DELETEALL:
                obstacle->obstacle3d_->setObstacleAction(ObstacleAction::delete_obstacle);
                break;
        }
    }

    // publish
    publish();


    // Clear the list with the removed obstacles
    //ROS_INFO("before list_obstacle_.size()=%d",list_obstacle_.size());
    for(std::list<ObstacleAssociation*>::iterator it_list_obstacle=list_obstacle_.begin();
        it_list_obstacle!=list_obstacle_.end();
        )
    {
        if((*it_list_obstacle)->obstacle3d_->getObstacleAction() == ObstacleAction::delete_obstacle)
        {
            it_list_obstacle=list_obstacle_.erase(it_list_obstacle);
        }
        else
        {
            ++it_list_obstacle;
        }

    }
    //ROS_INFO("after list_obstacle_.size()=%d",list_obstacle_.size());

    // end
    return;
}

int Obstacle2dConverterRos::publish()
{
    //ROS_INFO("publishing");

    // Checks
    if(list_obstacle_.empty() || !plane3d_.isOk())
        return -1;

    // Calculate list of obstacles2d
    for(std::list<ObstacleAssociation*>::iterator it_list_obstacle=list_obstacle_.begin();
        it_list_obstacle!=list_obstacle_.end();
        ++it_list_obstacle)
    {
        int error_convert=Obstacle2dConverter::convert((*it_list_obstacle)->obstacle3d_, plane3d_, (*it_list_obstacle)->obstacle2d_);
        if(error_convert<0)
        {
            // Error
            ROS_INFO("error_convert");
            continue;
        }
        else if(error_convert>0)
        {
            // No intersection
            //ROS_INFO("no intersection");
            continue;
        }
    }

    // prepare message
    //std::cout<<"list_obstacle_ size="<<list_obstacle_.size()<<std::endl;
    obstacles_2d_msg_.rectangles.clear();
    obstacles_2d_msg_.ellipses.clear();
    for(std::list<ObstacleAssociation*>::iterator it_list_obstacle=list_obstacle_.begin();
        it_list_obstacle!=list_obstacle_.end();
        ++it_list_obstacle)
    {
        // check
        if(!(*it_list_obstacle)->obstacle2d_)
        {
            //ROS_ERROR("no pointer in obstacle2d");
            continue;
        }

        // pose center
        Eigen::Vector2d position_center_wrt_world;
        double attitude_center_wrt_world;
        int error_get_pose_center=(*it_list_obstacle)->obstacle2d_->getPoseCenterWrtWorld(position_center_wrt_world, attitude_center_wrt_world);
        if(error_get_pose_center)
        {
            ROS_ERROR("error_get_pose_center");
            continue;
        }
        Eigen::Vector4d quaternion_attitude_center_wrt_world=Quaternion::eulerAnglesToQuaternion(attitude_center_wrt_world, 0, 0);;

        // others
        switch((*it_list_obstacle)->obstacle2d_->getObstacle2dType())
        {
            // rectangles
            case Obstacle2dTypes::rectangle2d:
            {
                Rectangle2d* rectangle2d=static_cast<Rectangle2d*>((*it_list_obstacle)->obstacle2d_);

                // Message
                environment_msgs::Rectangle rectangle_msg;

                // TODO fix!
                rectangle_msg.common.header.stamp=ros::Time::now();
                rectangle_msg.common.header.frame_id="world";

                rectangle_msg.common.ns="rectangle_"+std::to_string((*it_list_obstacle)->id_);

                rectangle_msg.common.id=(*it_list_obstacle)->id_;

                switch((*it_list_obstacle)->obstacle2d_->getObstacleAction())
                {
                    case ObstacleAction::update_obstacle:
                    default:
                        rectangle_msg.common.action=environment_msgs::Object2d::ADD;
                        break;
                    case ObstacleAction::delete_obstacle:
                        rectangle_msg.common.action=environment_msgs::Object2d::DELETE;
                        break;
                }
                

                rectangle_msg.common.lifetime=ros::Duration(0);

                // TODO Color

                // TODO fix!
                rectangle_msg.pose_center.position.x=position_center_wrt_world(0);
                rectangle_msg.pose_center.position.y=position_center_wrt_world(1);
                rectangle_msg.pose_center.position.z=0.0;
                
                rectangle_msg.pose_center.orientation.w=quaternion_attitude_center_wrt_world[0];
                rectangle_msg.pose_center.orientation.x=quaternion_attitude_center_wrt_world[1];;
                rectangle_msg.pose_center.orientation.y=quaternion_attitude_center_wrt_world[2];;
                rectangle_msg.pose_center.orientation.z=quaternion_attitude_center_wrt_world[3];;
                //rectangle_msgs.yawAngle=attitude_center_wrt_world;

                Eigen::Vector2d size;
                int error_get_size=rectangle2d->getSize(size);
                if(error_get_size)
                {
                    ROS_ERROR("error_get_size");
                    continue;
                }

                rectangle_msg.size_x=size(0);
                rectangle_msg.size_y=size(1);

                obstacles_2d_msg_.rectangles.push_back(rectangle_msg);

                break;
            }
            // ellipses
            case Obstacle2dTypes::ellipse2d:
            {
                Ellipse2d* ellipse2d=static_cast<Ellipse2d*>((*it_list_obstacle)->obstacle2d_);

                // Message
                environment_msgs::Ellipse ellipse_msg;

                // TODO fix!
                ellipse_msg.common.header.stamp=ros::Time::now();
                ellipse_msg.common.header.frame_id="world";

                ellipse_msg.common.ns="ellipse_"+std::to_string((*it_list_obstacle)->id_);

                ellipse_msg.common.id=(*it_list_obstacle)->id_;

                switch((*it_list_obstacle)->obstacle2d_->getObstacleAction())
                {
                    case ObstacleAction::update_obstacle:
                    default:
                        ellipse_msg.common.action=environment_msgs::Object2d::ADD;
                        break;
                    case ObstacleAction::delete_obstacle:
                        ellipse_msg.common.action=environment_msgs::Object2d::DELETE;
                        break;
                }

                ellipse_msg.common.lifetime=ros::Duration(0);

                // TODO Color

                // TODO fix!
                ellipse_msg.pose_center.position.x=position_center_wrt_world(0);
                ellipse_msg.pose_center.position.y=position_center_wrt_world(1);
                ellipse_msg.pose_center.position.z=0.0;
                
                ellipse_msg.pose_center.orientation.w=quaternion_attitude_center_wrt_world[0];
                ellipse_msg.pose_center.orientation.x=quaternion_attitude_center_wrt_world[1];
                ellipse_msg.pose_center.orientation.y=quaternion_attitude_center_wrt_world[2];
                ellipse_msg.pose_center.orientation.z=quaternion_attitude_center_wrt_world[3];
                //rectangle_msgs.yawAngle=attitude_center_wrt_world;

                Eigen::Vector2d radii;
                try
                {
                    radii=ellipse2d->getRadii();
                }
                catch(...)
                {
                    ROS_ERROR("error_get_radii");
                    continue;
                }

                ellipse_msg.radius_x=radii(0);
                ellipse_msg.radius_y=radii(1);

                obstacles_2d_msg_.ellipses.push_back(ellipse_msg);

                break;
            }
            default:
                break;
        }
    }

    // Publish
    obstacles_2d_pub_.publish(obstacles_2d_msg_);



    // end
    return 0;
}
