
//I/O stream
//std::cout
#include <iostream>


//
#include "obstacle2d_converter_ros/obstacle2d_converter.h"



int main(int argc,char **argv)
{
    Obstacle2dConverterRos obstacle2d_converter_ros(argc, argv);

    obstacle2d_converter_ros.open();

    obstacle2d_converter_ros.run();

    return 0;
}