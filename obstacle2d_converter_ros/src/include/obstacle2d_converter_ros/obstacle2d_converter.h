
#ifndef _TRAJECTORY_PLANNER2D_INTERFACE_H_
#define _TRAJECTORY_PLANNER2D_INTERFACE_H_



#include <string>


#include <Eigen/Dense>


#include <ros/ros.h>

#include <tf/transform_datatypes.h>




#include <geometry_msgs/PoseStamped.h>
//#include <geometry_msgs/PoseWithCovarianceStamped.h>




//#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/PoseStamped.h>
//#include <geometry_msgs/PoseWithCovarianceStamped.h>




#include <environment_msgs/Object2dArray.h>





#include <obstacle2d_converter_core/obstacle2d_converter.h>














class ObstacleAssociation
{
public:
    ~ObstacleAssociation();

public:
    int id_;
    Obstacle3d* obstacle3d_;
    Obstacle2d* obstacle2d_;

};




///////////////////////////////////////
/// \brief The Obstacle2dConverterRos class
////////////////////////////////////
class Obstacle2dConverterRos
{
public:
    Obstacle2dConverterRos(int argc,char **argv);
    ~Obstacle2dConverterRos();


protected:
    ros::NodeHandle* nh_;

protected:
    Plane3d plane3d_;
    std::list<ObstacleAssociation*> list_obstacle_;

protected:
    std::string pose_topic_name_;
    ros::Subscriber pose_sub_;
    void poseCallback(const geometry_msgs::PoseStamped::ConstPtr &msg);
protected:
    std::string obstacles_3d_topic_name_;
    ros::Subscriber obstacles_3d_sub_;
    void obstacles3dCallback(const visualization_msgs::MarkerArray::ConstPtr &msg);
protected:
    std::string obstacles_2d_topic_name_;
    ros::Publisher obstacles_2d_pub_;
    environment_msgs::Object2dArray obstacles_2d_msg_;

protected:
    void readParameters();

public:
    int open();


public:
    int run();

protected:
    int publish();



};











#endif
