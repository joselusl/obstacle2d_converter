
#include "obstacle2d_converter_core/obstacle2d_converter.h"




Obstacle::Obstacle()
{
    obstacle_action_=ObstacleAction::undefined;

    return;
}

Obstacle::~Obstacle()
{

}

ObstacleAction Obstacle::getObstacleAction() const
{
    return obstacle_action_;
}

void Obstacle::setObstacleAction(ObstacleAction obstacle_action)
{
    obstacle_action_=obstacle_action;
    return;
}






Obstacle3d::Obstacle3d()
{
    obstacle3d_type_=Obstacle3dTypes::undefined;
    flag_pose_center_wrt_world_=false;
    return;
}

Obstacle3d::~Obstacle3d()
{
    return;
}

Obstacle3dTypes Obstacle3d::getObstacle3dType() const
{
    return obstacle3d_type_;
}

void Obstacle3d::setPoseCenterWrtWorld(const Eigen::Vector3d& position_center_wrt_world,
                            const Eigen::Vector4d& attitude_center_wrt_world)
{
    position_center_wrt_world_=position_center_wrt_world;
    attitude_center_wrt_world_=attitude_center_wrt_world;
    flag_pose_center_wrt_world_=true;
    return;
}

Eigen::Vector3d Obstacle3d::getPositionCenterWrtWorld() const
{
    if(!flag_pose_center_wrt_world_)
        throw 1;
    return position_center_wrt_world_;
}

Eigen::Vector4d Obstacle3d::getAttitudeCenterWrtWorld() const
{
    if(!flag_pose_center_wrt_world_)
        throw 1;
    return attitude_center_wrt_world_;
}





Cuboid3d::Cuboid3d()
{
    init();

    return;
}

Cuboid3d::Cuboid3d(const Eigen::Vector3d& position_center_wrt_world,
       const Eigen::Vector4d& attitude_center_wrt_world,
       const Eigen::Vector3d& size)
{
    init();
    setPoseCenterWrtWorld(position_center_wrt_world, attitude_center_wrt_world);
    setSize(size);
    calculateVertices();
    return;
}

Cuboid3d::~Cuboid3d()
{
    return;
}

void Cuboid3d::init()
{
    obstacle3d_type_=Obstacle3dTypes::cube3d;
    flag_size_=false;
    flag_vertices_=false;
    flag_edges_=false;

    return;
}

void Cuboid3d::setSize(const Eigen::Vector3d& size)
{
    size_=size;
    flag_size_=true;
    return;
}

int Cuboid3d::getNumVertices() const
{
    if(!flag_vertices_)
        return -1;
    return vertices_wrt_cube_frame_.size();
}

int Cuboid3d::calculateVertices()
{
    // check
    if(!flag_size_ || !flag_pose_center_wrt_world_)
        return -1;

    // vertices wrt cube center
    vertices_wrt_cube_frame_.resize(8);
    vertices_wrt_cube_frame_[0]=Eigen::Vector3d(0,0,0);
    vertices_wrt_cube_frame_[1]=Eigen::Vector3d(size_(0),0,0);
    vertices_wrt_cube_frame_[2]=Eigen::Vector3d(0,size_(1),0);
    vertices_wrt_cube_frame_[3]=Eigen::Vector3d(0,0,size_(2));
    vertices_wrt_cube_frame_[4]=Eigen::Vector3d(size_(0),0,size_(2));
    vertices_wrt_cube_frame_[5]=Eigen::Vector3d(size_(0),size_(1),0);
    vertices_wrt_cube_frame_[6]=Eigen::Vector3d(0,size_(1),size_(2));
    vertices_wrt_cube_frame_[7]=Eigen::Vector3d(size_(0),size_(1),size_(2));

    // edges
    edges_.resize(12);
    edges_[0]=Eigen::Vector2i(0,1);
    edges_[1]=Eigen::Vector2i(1,4);
    edges_[2]=Eigen::Vector2i(4,7);
    edges_[3]=Eigen::Vector2i(1,5);
    edges_[4]=Eigen::Vector2i(0,2);
    edges_[5]=Eigen::Vector2i(2,5);
    edges_[6]=Eigen::Vector2i(5,7);
    edges_[7]=Eigen::Vector2i(2,6);
    edges_[8]=Eigen::Vector2i(0,3);
    edges_[9]=Eigen::Vector2i(3,6);
    edges_[10]=Eigen::Vector2i(6,7);
    edges_[11]=Eigen::Vector2i(3,4);

    // vertices wrt world
    vertices_wrt_world_.resize(8);
    Eigen::Vector3d position_center_wrt_cube_ref_frame=size_/2;
    for(int i=0; i<8; i++)
    {
        vertices_wrt_world_[i]=Quaternion::cross_sandwich(attitude_center_wrt_world_, vertices_wrt_cube_frame_[i]-position_center_wrt_cube_ref_frame, Quaternion::conj(attitude_center_wrt_world_))
                +position_center_wrt_world_;
    }

    flag_vertices_=true;
    flag_edges_=true;

    // end
    return 0;
}

int Cuboid3d::getVerticesWrtWorld(std::vector<Eigen::Vector3d>& vertices_wrt_world) const
{
    if(!flag_vertices_)
        return -1;
    vertices_wrt_world=vertices_wrt_world_;
    return 0;
}

int Cuboid3d::getNumEdges() const
{
    if(!flag_edges_)
        return -1;
    return edges_.size();
}

int Cuboid3d::getVerticesOfEdgeWrtWorld(int edge_i, Eigen::Vector3d& vertix1, Eigen::Vector3d& vertix2) const
{
    if(!flag_vertices_ || ! flag_edges_)
        return -1;
    vertix1=vertices_wrt_world_[edges_[edge_i](0)];
    vertix2=vertices_wrt_world_[edges_[edge_i](1)];
    return 0;
}

bool Cuboid3d::isOk() const
{
    return (flag_pose_center_wrt_world_ && flag_size_ && flag_vertices_ && flag_edges_);
}






Cylinder3d::Cylinder3d()
{
    init();
    return;
}

Cylinder3d::Cylinder3d(const Eigen::Vector3d& position_center_wrt_world,
       const Eigen::Vector4d& attitude_center_wrt_world,
       const Eigen::Vector2d& radii,
       double height)
{
    init();
    setPoseCenterWrtWorld(position_center_wrt_world, attitude_center_wrt_world);
    setRadii(radii);
    setHeight(height);

    return;
}

Cylinder3d::~Cylinder3d()
{
    return;
}

void Cylinder3d::init()
{
    obstacle3d_type_=Obstacle3dTypes::cylinder3d;
    flag_radii_=false;
    flag_height_=false;

    return;
}

void Cylinder3d::setRadii(const Eigen::Vector2d& radii)
{
    radii_=radii;
    flag_radii_=true;
    return;
}

Eigen::Vector2d Cylinder3d::getRadii() const
{
    if(!flag_radii_)
        throw 1;
    return radii_;
}

void Cylinder3d::setHeight(const double height)
{
    height_=height;
    flag_height_=true;
    return;
}

double Cylinder3d::getHeight() const
{
    if(!flag_height_)
        throw 1;
    return height_;
}

void Cylinder3d::setParameters(const Eigen::Vector2d& radii, const double height)
{
    setRadii(radii);
    setHeight(height);
    return;
}

bool Cylinder3d::isOk() const
{
    return (flag_pose_center_wrt_world_ && flag_radii_ && flag_height_);
}








Plane3d::Plane3d()
{
    init();
    return;
}

Plane3d::Plane3d(const Eigen::Vector3d& normal, double distance)
{
    init();
    setPlane3d(normal, distance);
    return;
}

void Plane3d::init()
{
    flag_plane_definition_=false;
    return;
}

bool Plane3d::isOk() const
{
    return (flag_plane_definition_);
}

void Plane3d::setPlane3d(const Eigen::Vector3d& normal, double distance)
{
    normal_=normal;
    distance_=distance;
    flag_plane_definition_=true;
    return;
}

int Plane3d::getPlane3d(Eigen::Vector3d& normal, double& distance) const
{
    if(!flag_plane_definition_)
        return -1;
    normal=normal_;
    distance=distance_;
    return 0;
}






Obstacle2d::Obstacle2d()
{
    init();
    return;
}

Obstacle2d::~Obstacle2d()
{
    return;
}

int Obstacle2d::init()
{
    obstacle2d_type_=Obstacle2dTypes::undefined;
    flag_pose_center_=false;
    return 0;
}

void Obstacle2d::setObstacle2dType(Obstacle2dTypes obstacle2d_type)
{
    obstacle2d_type_=obstacle2d_type;
    return;
}

Obstacle2dTypes Obstacle2d::getObstacle2dType() const
{
    return obstacle2d_type_;
}

void Obstacle2d::setPoseCenterWrtWorld(const Eigen::Vector2d& position_center_wrt_world, double attitude_center_wrt_world)
{
    position_center_wrt_world_=position_center_wrt_world;
    attitude_center_wrt_world_=attitude_center_wrt_world;
    flag_pose_center_=true;
    return;
}

int Obstacle2d::getPoseCenterWrtWorld(Eigen::Vector2d& position_center_wrt_world, double& attitude_center_wrt_world) const
{
    if(!flag_pose_center_)
        return -1;
    position_center_wrt_world=position_center_wrt_world_;
    attitude_center_wrt_world=attitude_center_wrt_world_;
    return 0;
}

Eigen::Vector2d Obstacle2d::getPositionCenterWrtWorld() const
{
    if(!flag_pose_center_)
    {
        std::cerr<<"Pose not properly set"<<std::endl;
        throw 1;
    }
    return position_center_wrt_world_;
}

double Obstacle2d::getAttitudeCenterWrtWorld() const
{
    if(!flag_pose_center_)
    {
        std::cerr<<"Pose not properly set"<<std::endl;
        throw 1;
    }
    return attitude_center_wrt_world_;
}






Rectangle2d::Rectangle2d()
{
    init();
    return;
}

Rectangle2d::~Rectangle2d()
{
    return;
}

int Rectangle2d::init()
{
    obstacle2d_type_=Obstacle2dTypes::rectangle2d;
    flag_size_=false;
    size_.setOnes();
    size_*=-1;
    flag_edges_=false;
    flag_vertices_=false;
    return 0;
}

void Rectangle2d::setSize(const Eigen::Vector2d& size)
{
    size_=size;
    flag_size_=true;
    return;
}

int Rectangle2d::getSize(Eigen::Vector2d& size) const
{
    if(!flag_size_)
        return -1;
    size=size_;
    return 0;
}

void Rectangle2d::setSizeI(int i, double size_i)
{
    size_(i)=size_i;
    if(!flag_size_ && size_(0)>0 && size_(1)>0)
        flag_size_=true;
    return;
}

int Rectangle2d::getSizeI(int i, double &size_i) const
{
    if(!flag_size_ && size_(i)<0)
        return -1;
    size_i=size_(i);
    return 0;
}

void Rectangle2d::setVertices(const std::vector<Eigen::Vector2d>& vertices)
{
    vertices_=vertices;
    flag_vertices_=true;
    return;
}

int Rectangle2d::getVertices(std::vector<Eigen::Vector2d>& vertices) const
{
    if(!flag_vertices_)
        return -1;
    vertices=vertices_;
    return 0;
}

void Rectangle2d::setEdges(const std::vector<Eigen::Vector2i>& edges)
{
    edges_=edges;
    flag_edges_=true;
    return;
}

int Rectangle2d::getEdges(std::vector<Eigen::Vector2i>& edges) const
{
    if(!flag_edges_)
        return -1;
    edges=edges_;
    return 0;
}

int Rectangle2d::getArea(double& area) const
{
    if(!flag_size_)
        return -1;
    area=size_(0)*size_(1);
    return 0;
}

int Rectangle2d::computePositionCenterFromVertices()
{
    if(!flag_vertices_)
        return -1;
    position_center_wrt_world_.setZero();
    for(int i=0; i<vertices_.size(); i++)
    {
        position_center_wrt_world_+=vertices_[i];
    }
    position_center_wrt_world_/=vertices_.size();
    return 0;
}

int Rectangle2d::computeAttitudeCenterFromVertices()
{
    if(!flag_vertices_)
        return -1;
    //Eigen::Vector2d vertices_diff=(vertices_[0]-vertices_[1]);
    double value1=((vertices_[0]-vertices_[1]).transpose()*Eigen::Vector2d(1,0));
    attitude_center_wrt_world_=std::acos(value1/(size_(0)));
    // std::cout<<"attitude_center_wrt_world_="<<attitude_center_wrt_world_<<std::endl;
    return 0;
}

int Rectangle2d::computePoseCenterFromVertices()
{
    if(computePositionCenterFromVertices())
        return -1;
    if(computeAttitudeCenterFromVertices())
        return -2;
    flag_pose_center_=true;
    return 0;
}

int Rectangle2d::copyFrom(const Rectangle2d& rectangle)
{
    // Obstacle Type
    obstacle2d_type_=rectangle.getObstacle2dType();

    // Pose Center
    Eigen::Vector2d position_center_wrt_world;
    double attitude_center_wrt_world;
    int error_pose_center=rectangle.getPoseCenterWrtWorld(position_center_wrt_world, attitude_center_wrt_world);
    if(!error_pose_center)
        setPoseCenterWrtWorld(position_center_wrt_world, attitude_center_wrt_world);

    // Size
    Eigen::Vector2d size;
    int error_size=rectangle.getSize(size);
    if(!error_size)
        setSize(size);

    // Vertices
    std::vector<Eigen::Vector2d> vertices;
    int error_vertices=rectangle.getVertices(vertices);
    if(!error_vertices)
        setVertices(vertices);

    // Edges
    std::vector<Eigen::Vector2i> edges;
    int error_edges=rectangle.getEdges(edges);
    if(!error_edges)
        setEdges(edges);

    // end
    return 0;
}

int Rectangle2d::copyTo(Rectangle2d& rectangle) const
{
    // Obstacle Type
    rectangle.setObstacle2dType(getObstacle2dType());

    // Pose Center
    Eigen::Vector2d position_center_wrt_world;
    double attitude_center_wrt_world;
    int error_pose_center=getPoseCenterWrtWorld(position_center_wrt_world, attitude_center_wrt_world);
    if(!error_pose_center)
        rectangle.setPoseCenterWrtWorld(position_center_wrt_world, attitude_center_wrt_world);

    // Size
    Eigen::Vector2d size;
    int error_size=getSize(size);
    if(!error_size)
        rectangle.setSize(size);

    // Vertices
    std::vector<Eigen::Vector2d> vertices;
    int error_vertices=getVertices(vertices);
    if(!error_vertices)
        rectangle.setVertices(vertices);

    // Edges
    std::vector<Eigen::Vector2i> edges;
    int error_edges=getEdges(edges);
    if(!error_edges)
        rectangle.setEdges(edges);

    // end
    return 0;
}






Ellipse2d::Ellipse2d()
{
    init();
    return;
}

Ellipse2d::~Ellipse2d()
{
    return;
}

int Ellipse2d::init()
{
    obstacle2d_type_=Obstacle2dTypes::ellipse2d;
    flag_radii_=false;
    return 0;
}

void Ellipse2d::setRadii(const Eigen::Vector2d& radii)
{
    radii_=radii;
    flag_radii_=true;
    return;
}

Eigen::Vector2d Ellipse2d::getRadii() const
{
    if(!flag_radii_)
        throw 1;
    return radii_;
}








int Obstacle2dConverter::convert(const Obstacle3d *obstacle3d, const Plane3d &plane3d, Obstacle2d*& obstacle2d)
{
    // Check
    if(!obstacle3d)
        return -1;
    if(!obstacle3d->isOk() || !plane3d.isOk())
        return -1;


    // obstacle3d is a cube
    switch(obstacle3d->getObstacle3dType())
    {
        // obstacle3d is a cube
        case Obstacle3dTypes::cube3d:
        {
            // Polymorphism
            const Cuboid3d* cuboid3d=static_cast<const Cuboid3d*>(obstacle3d);

            // Rectanlge
            Rectangle2d* rectangle2d = static_cast<Rectangle2d*>(obstacle2d);

            // Convert cuboid3d -> rectangle2d
            if(convert(cuboid3d, plane3d, rectangle2d))
                return -1;

            // Set
            obstacle2d=rectangle2d;
            
            // End
            break;
        }
        // Obstacle 3d is a cylinder
        case Obstacle3dTypes::cylinder3d:
        {
            // Polymorphism
            const Cylinder3d* cylinder3d=static_cast<const Cylinder3d*>(obstacle3d);

            // Convert cylinder3d -> ellipse2d or rectangle2d
            if(convert(cylinder3d, plane3d, obstacle2d))
                return -1;

            // End
            break;
        }
        // No defined
        default:
            return -2;
            break;
    }

    // Set action if it is to delete
    if(obstacle3d->getObstacleAction() == ObstacleAction::delete_obstacle)
        obstacle2d->setObstacleAction(ObstacleAction::delete_obstacle);


    // End
    return 0;
}

int Obstacle2dConverter::convert(const Cuboid3d* cuboid3d, const Plane3d& plane3d, Rectangle2d*& rectangle2d)
{
    // Minimum-area enclosing rectangle
    //std::cout<<"Minimum-area enclosing rectangle"<<std::endl;
    if(!rectangle2d)
    {
        rectangle2d=new Rectangle2d();
    }

    // Calculate 3d interections
    Eigen::Vector3d plane_normal;
    double plane_distance;
    int error_get_plane=plane3d.getPlane3d(plane_normal, plane_distance);
    if(error_get_plane)
        return -1;

    Eigen::Vector3d vertix1;
    Eigen::Vector3d vertix2;

    std::vector<Eigen::Vector3d> list_point3d_intersection;

    for(int edge_i=0; edge_i<cuboid3d->getNumEdges(); edge_i++)
    {
        int error_get_vertices_edge=cuboid3d->getVerticesOfEdgeWrtWorld(edge_i, vertix1, vertix2);
        if(error_get_vertices_edge)
            return -1;

        double lambda=-(plane_distance+plane_normal.transpose()*vertix1)/(plane_normal.transpose()*(vertix2-vertix1));

        if(lambda>=0 && lambda<=1)
        {
            Eigen::Vector3d point_intersection;
            point_intersection=vertix1+lambda*(vertix2-vertix1);
            list_point3d_intersection.push_back(point_intersection);
        }
    }

    // Check
    int num_point_intersection=list_point3d_intersection.size();
    if(num_point_intersection<3)
    {
        rectangle2d->setObstacleAction(ObstacleAction::delete_obstacle);
        return 1;
    }

    // Order 3d intersections [Simple polygonization]
    bool flag_polygon_ordered;
    // First ordering
    flag_polygon_ordered=false;
    while(!flag_polygon_ordered)
    {
        flag_polygon_ordered=true;

        for(int i=0; i<num_point_intersection; i++)
        {
            int prim, seco, terc;
            if(i==0)
                prim=num_point_intersection-1;
            else
                prim=i-1;
            seco=i;
            if(i==num_point_intersection-1)
                terc=0;
            else
                terc=i+1;

            Eigen::Vector3d edge1=list_point3d_intersection[seco]-list_point3d_intersection[prim];
            Eigen::Vector3d edge2=list_point3d_intersection[terc]-list_point3d_intersection[seco];

            Eigen::Vector3d val=edge1.cross(edge2);

            if(val(3)>=0)
            {
                // do nothing
            }
            else
            {
                // swap points
                Eigen::Vector3d aux=list_point3d_intersection[seco];
                list_point3d_intersection[seco]=list_point3d_intersection[terc];
                list_point3d_intersection[terc]=aux;
                //
                flag_polygon_ordered=false;
                break;
            }
        }
    }
    // Second ordering
    flag_polygon_ordered=false;
    Eigen::Vector3d point3d_intersection_centroid;
    point3d_intersection_centroid.setZero();
    for(int i=0; i<num_point_intersection; i++)
    {
        point3d_intersection_centroid+=list_point3d_intersection[i];
    }
    point3d_intersection_centroid/=num_point_intersection;
    while(!flag_polygon_ordered)
    {
        flag_polygon_ordered=true;

        for(int i=0; i<num_point_intersection; i++)
        {
            int prim, terc;
            prim=i;
            if(i==num_point_intersection-1)
                terc=0;
            else
                terc=i+1;

            Eigen::Vector3d edge1=list_point3d_intersection[prim]-point3d_intersection_centroid;
            Eigen::Vector3d edge2=point3d_intersection_centroid-list_point3d_intersection[terc];

            Eigen::Vector3d val=edge1.cross(edge2);

            if(val(3)>=0)
            {
                // do nothing
            }
            else
            {
                // swap points
                Eigen::Vector3d aux=list_point3d_intersection[prim];
                list_point3d_intersection[prim]=list_point3d_intersection[terc];
                list_point3d_intersection[terc]=aux;
                //
                flag_polygon_ordered=false;
                break;
            }
        }
    }

//            for(int ii=0;ii<num_point_intersection;ii++)
//            {
//                std::cout<<"poly3d_vertix["<<ii<<"]="<<list_point3d_intersection[ii].transpose()<<std::endl;
//            }


    // Create obstacle 2d
    // edges
    std::vector<Eigen::Vector2i> edges_polygon;
    edges_polygon.push_back(Eigen::Vector2i(0,num_point_intersection-1));
    for(int i=1; i<num_point_intersection; i++)
    {
        edges_polygon.push_back(Eigen::Vector2i(i-1,i));
    }

    // Pose Projected ref frame of the plane
    // Attitude
    Eigen::Vector3d ground_normal(0,0,1);
    Eigen::Vector3d plane_normal_unit=plane_normal/plane_normal.norm();
    Eigen::Vector3d sum_normal=ground_normal+plane_normal_unit;
    Eigen::Vector3d sum_normal_unit=sum_normal/sum_normal.norm();
    Eigen::Vector3d attitude_axis_plane_frame_wrt_world=sum_normal_unit.cross(plane_normal_unit);
    double attitude_angle_plane_frame_wrt_world=sum_normal_unit.transpose()*plane_normal_unit;
    Eigen::Vector4d attitude_aux_plane_frame_wrt_world(attitude_angle_plane_frame_wrt_world, attitude_axis_plane_frame_wrt_world(0), attitude_axis_plane_frame_wrt_world(1), attitude_axis_plane_frame_wrt_world(2));
    Eigen::Vector4d attitude_plane_frame_wrt_world=attitude_aux_plane_frame_wrt_world/attitude_aux_plane_frame_wrt_world.norm();
    // Position
    Eigen::Vector3d position_plane_frame_wrt_world=(-plane_distance/(plane_normal.transpose()*plane_normal))*plane_normal;

    // Points 2d wrt proj ref frame
    std::vector<Eigen::Vector2d> list_point2d_intersection;
    for(int i=0; i<num_point_intersection; i++)
    {
        Eigen::Vector3d point2d_intersection=Quaternion::cross_sandwich(Quaternion::conj(attitude_plane_frame_wrt_world), (list_point3d_intersection[i]-position_plane_frame_wrt_world), attitude_plane_frame_wrt_world);
        list_point2d_intersection.push_back(Eigen::Vector2d(point2d_intersection(0),point2d_intersection(1)));
    }


    // Minimum-area enclosing rectangle
    //std::cout<<"Minimum-area enclosing rectangle"<<std::endl;
    // if(!rectangle2d)
    // {
    //     rectangle2d=new Rectangle2d();
    // }

    Rectangle2d rectangle;
    double rectangle_area=std::numeric_limits<double>::infinity();
    for(int i=0; i<num_point_intersection; i++)
    {
        // Rectangle aux
        double rectangle_aux_area=std::numeric_limits<double>::infinity();
        Rectangle2d rectangle_aux;
        rectangle_aux.setSize(Eigen::Vector2d(0,0));
        std::vector<Eigen::Vector2d> rectangle_aux_vertices;
        rectangle_aux_vertices.resize(4);

        // Edge
        Eigen::Vector2d edge1=list_point2d_intersection[edges_polygon[i](0)]-list_point2d_intersection[edges_polygon[i](1)];
        edge1/=edge1.norm();


        // Width
        for(int ii=0; ii<num_point_intersection; ii++)
        {
            Eigen::Vector2d edge2=list_point2d_intersection[ii]-list_point2d_intersection[edges_polygon[i](1)];
            for(int jj=0; jj<num_point_intersection; jj++)
            {
                Eigen::Vector2d edge3=list_point2d_intersection[jj]-list_point2d_intersection[edges_polygon[i](1)];

                Eigen::Vector2d proj1=list_point2d_intersection[edges_polygon[i](1)]+edge1.dot(edge2)*edge1;
                Eigen::Vector2d proj2=list_point2d_intersection[edges_polygon[i](1)]+edge1.dot(edge3)*edge1;

                double width=(proj2-proj1).norm();

                double rectangle_aux_width;
                int error_get_size=rectangle_aux.getSizeI(0, rectangle_aux_width);
                if(width>rectangle_aux_width)
                {
                    rectangle_aux.setSizeI(0, width);
                    rectangle_aux_vertices[0]=proj1;
                    rectangle_aux_vertices[1]=proj2;
                }
            }
        }


        // Axis
        double coeff1=rectangle_aux_vertices[1](0)-rectangle_aux_vertices[0](0);
        if(coeff1==0)
            continue;

        double rectangle_axis1_m=(rectangle_aux_vertices[1](1)-rectangle_aux_vertices[0](1))/(rectangle_aux_vertices[1](0)-rectangle_aux_vertices[0](0));
        double rectangle_axis1_a=-rectangle_axis1_m;
        double rectangle_axis1_b=1;
        double rectangle_axis1_c=rectangle_axis1_m*rectangle_aux_vertices[0](0)-rectangle_aux_vertices[0](1);


        // Height
        for(int ii=0; ii<num_point_intersection; ii++)
        {

            if(std::pow(rectangle_axis1_a,2)+std::pow(rectangle_axis1_b,2) == 0)
                continue;

            double dist_i=std::abs(rectangle_axis1_a*list_point2d_intersection[ii](0)+rectangle_axis1_b*list_point2d_intersection[ii](1)+rectangle_axis1_c)/std::sqrt(std::pow(rectangle_axis1_a,2)+std::pow(rectangle_axis1_b,2));

            double rectangle_aux_height;
            int error_get_size=rectangle_aux.getSizeI(1, rectangle_aux_height);

            if(dist_i > rectangle_aux_height)
            {
                rectangle_aux.setSizeI(1, dist_i);

                Eigen::Vector2d edge2=list_point2d_intersection[ii]-list_point2d_intersection[edges_polygon[i](1)];
                if(edge2.norm()==0)
                    continue;
                edge2/=edge2.norm();

                Eigen::Vector2d edge_perp=edge2-edge2.dot(edge1)*edge1;
                if(edge_perp.norm()==0)
                    continue;
                edge_perp/=edge_perp.norm();

                rectangle_aux_vertices[2]=rectangle_aux_vertices[0]+dist_i*edge_perp;
                rectangle_aux_vertices[3]=rectangle_aux_vertices[1]+dist_i*edge_perp;
            }

        }

        // Set rectangle aux vertices
        rectangle_aux.setVertices(rectangle_aux_vertices);


        // Area
        int error_get_area=rectangle_aux.getArea(rectangle_aux_area);
        if(error_get_area)
            return -1;

        // check
        if(rectangle_aux_area < rectangle_area)
        {
            rectangle.copyFrom(rectangle_aux);
        }
    }


    // Set rectangle
    int error_compute_pose_center=rectangle.computePoseCenterFromVertices();
    if(error_compute_pose_center)
        return -1;


    // Set output value
    if(rectangle.copyTo(*rectangle2d))
    {
        std::cerr<<"Error copyTo()"<<std::endl;
        throw 1;
    }


    // std::vector<Eigen::Vector2d> rectangle_vertices;
    // rectangle.getVertices(rectangle_vertices);
    // for(int ii=0;ii<rectangle_vertices.size();ii++)
    // {
    //     std::cout<<"rectangle_vertices["<<ii<<"]="<<rectangle_vertices[ii].transpose()<<std::endl;
    // }


    // Add action
    rectangle2d->setObstacleAction(ObstacleAction::update_obstacle);

    // End
    return 0;
}

int Obstacle2dConverter::convert(const Cylinder3d* cylinder3d, const Plane3d& plane3d, Obstacle2d*& obstacle2d)
{
    // Cylinder center
    Eigen::Vector3d position_center_cylinder_wrt_world=cylinder3d->getPositionCenterWrtWorld();
    Eigen::Vector4d attitude_center_cylinder_wrt_world=cylinder3d->getAttitudeCenterWrtWorld();

    // Cylinder parameters
    double height_cyliner=cylinder3d->getHeight();
    Eigen::Vector2d radii_cylinder=cylinder3d->getRadii();


    // Plane
    Eigen::Vector3d plane_normal;
    double plane_distance;
    int error_get_plane=plane3d.getPlane3d(plane_normal, plane_distance);
    if(error_get_plane)
        return -1;


    // Pose Projected ref frame of the plane
    // Attitude
    Eigen::Vector3d ground_normal(0,0,1);
    Eigen::Vector3d plane_normal_unit=plane_normal/plane_normal.norm();
    Eigen::Vector3d sum_normal=ground_normal+plane_normal_unit;
    Eigen::Vector3d sum_normal_unit=sum_normal/sum_normal.norm();
    Eigen::Vector3d attitude_axis_plane_frame_wrt_world=sum_normal_unit.cross(plane_normal_unit);
    double attitude_angle_plane_frame_wrt_world=sum_normal_unit.transpose()*plane_normal_unit;
    Eigen::Vector4d attitude_aux_plane_frame_wrt_world(attitude_angle_plane_frame_wrt_world, attitude_axis_plane_frame_wrt_world(0), attitude_axis_plane_frame_wrt_world(1), attitude_axis_plane_frame_wrt_world(2));
    Eigen::Vector4d attitude_plane_frame_wrt_world=attitude_aux_plane_frame_wrt_world/attitude_aux_plane_frame_wrt_world.norm();
    // Position
    Eigen::Vector3d position_plane_frame_wrt_world=(-plane_distance/(plane_normal.transpose()*plane_normal))*plane_normal;



    // Hyp 1: obstacle 2d is an ellipse
    Ellipse2d* ellipse2d;
    if(!obstacle2d) 
    {   
        ellipse2d = new Ellipse2d;
        obstacle2d=static_cast<Ellipse2d*>(ellipse2d);
    }
    else
    {
        if(obstacle2d->getObstacle2dType() == Obstacle2dTypes::ellipse2d)
        {
            ellipse2d=static_cast<Ellipse2d*>(obstacle2d);
        }
        else
        {
            // Delete
            delete obstacle2d;
            // Create new
            ellipse2d = new Ellipse2d;
            obstacle2d=static_cast<Ellipse2d*>(ellipse2d);
        }
    }
    

    // TODO FIX!!
    // Ellipse center
    Eigen::Vector2d position_center_ellipse_wrt_world;
    double attitude_center_ellipse_wrt_world;
    position_center_ellipse_wrt_world[0]=position_center_cylinder_wrt_world[0];
    position_center_ellipse_wrt_world[1]=position_center_cylinder_wrt_world[1];
    attitude_center_ellipse_wrt_world = 0.0;
    ellipse2d->setPoseCenterWrtWorld(position_center_ellipse_wrt_world, attitude_center_ellipse_wrt_world);

    // Ellipse parameters
    Eigen::Vector2d radii_ellipse;
    radii_ellipse=radii_cylinder;
    ellipse2d->setRadii(radii_ellipse);

    // Add action
    ellipse2d->setObstacleAction(ObstacleAction::update_obstacle);


    // Hyp 2: obstacle 2d is a rectangle


    // End
    return 0;
}