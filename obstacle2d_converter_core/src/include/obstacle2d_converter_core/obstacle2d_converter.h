
#ifndef _OBSTACLE2D_CONVERTER_H_
#define _OBSTACLE2D_CONVERTER_H_


#include <iostream>

#include <vector>

#include <list>

#include <limits>


#include <Eigen/Dense>

#include <quaternion_algebra/quaternion_algebra.h>



enum class ObstacleAction
{
    undefined=0,
    update_obstacle,
    delete_obstacle
};


enum class Obstacle3dTypes
{
    undefined=0,
    cube3d,
    cylinder3d,
    sphere3d
};



enum class Obstacle2dTypes
{
    undefined=0,
    rectangle2d,
    ellipse2d

};




////////////////////////
/// \brief The Obstacle class
////////////////////////
class Obstacle
{
public:
    Obstacle();
    virtual ~Obstacle();

protected:
    ObstacleAction obstacle_action_;
public:
    ObstacleAction getObstacleAction() const;
    void setObstacleAction(ObstacleAction obstacle_action);



};






////////////////////////
/// \brief The Obstacle3d class
////////////////////////
class Obstacle3d : public Obstacle
{
public:
    Obstacle3d();
    virtual ~Obstacle3d();

protected:
    Obstacle3dTypes obstacle3d_type_;
public:
    Obstacle3dTypes getObstacle3dType() const;




protected:
    Eigen::Vector3d position_center_wrt_world_;
    Eigen::Vector4d attitude_center_wrt_world_;
    bool flag_pose_center_wrt_world_;
public:
    void setPoseCenterWrtWorld(const Eigen::Vector3d& position_center_wrt_world,
                                const Eigen::Vector4d& attitude_center_wrt_world);
    Eigen::Vector3d getPositionCenterWrtWorld() const;
    Eigen::Vector4d getAttitudeCenterWrtWorld() const;

public:
    virtual bool isOk() const{return false;};

};



//////////////////////////
/// \brief The Cube3d class
//////////////////////////
class Cuboid3d : public Obstacle3d
{
public:
    Cuboid3d();
    Cuboid3d(const Eigen::Vector3d& position_center_wrt_world,
           const Eigen::Vector4d& attitude_center_wrt_world,
           const Eigen::Vector3d& size);
    ~Cuboid3d();
protected:
    void init();

protected:
    Eigen::Vector3d size_;
    bool flag_size_;
public:
    void setSize(const Eigen::Vector3d& size);

private:
    std::vector<Eigen::Vector3d> vertices_wrt_cube_frame_;
    std::vector<Eigen::Vector3d> vertices_wrt_world_;
    bool flag_vertices_;
public:
    int getNumVertices() const;
public:
    int calculateVertices();
public:
    int getVerticesWrtWorld(std::vector<Eigen::Vector3d>& vertices_wrt_world) const;

private:
    std::vector<Eigen::Vector2i> edges_;
    bool flag_edges_;
public:
    int getNumEdges() const;

public:
    int getVerticesOfEdgeWrtWorld(int edge_i, Eigen::Vector3d& vertix1, Eigen::Vector3d& vertix2) const;

public:
    bool isOk() const;

};




////////////////////////////////
/// \brief The Cylinder3d class
///////////////////////////////
class Cylinder3d : public Obstacle3d
{
public:
    Cylinder3d();
    Cylinder3d(const Eigen::Vector3d& position_center_wrt_world,
       const Eigen::Vector4d& attitude_center_wrt_world,
       const Eigen::Vector2d& radii,
       double height);
    ~Cylinder3d();

protected:
    void init();

protected:
    Eigen::Vector2d radii_;
    bool flag_radii_;
public:
    void setRadii(const Eigen::Vector2d& radii);
    Eigen::Vector2d getRadii() const;

protected:
    double height_;
    bool flag_height_;
public:
    void setHeight(const double height);
    double getHeight() const; 

public:
    void setParameters(const Eigen::Vector2d& radii, const double height);


public:
    bool isOk() const;

};





//////////////////////////////////
/// \brief The Sphere3d class
/////////////////////////////////
class Sphere3d : public Obstacle3d
{

};







//////////////////////////////////
/// \brief The Plane3d class
//////////////////////////////////
class Plane3d
{
public:
    Plane3d();
    Plane3d(const Eigen::Vector3d& normal, double distance);

protected:
    void init();

public:
    bool isOk() const;

protected:
    Eigen::Vector3d normal_;
    double distance_;
    bool flag_plane_definition_;

public:
    void setPlane3d(const Eigen::Vector3d& normal, double distance);
    int getPlane3d(Eigen::Vector3d& normal, double& distance) const;



};








/////////////////////////////
/// \brief The Obstacle2d class
////////////////////////////
class Obstacle2d : public Obstacle
{
public:
    Obstacle2d();
    virtual ~Obstacle2d();
protected:
    int init();

protected:
    Obstacle2dTypes obstacle2d_type_;
public:
    void setObstacle2dType(Obstacle2dTypes obstacle2d_type);
    Obstacle2dTypes getObstacle2dType() const;

protected:
    Eigen::Vector2d position_center_wrt_world_;
    double attitude_center_wrt_world_;
    bool flag_pose_center_;
public:
    void setPoseCenterWrtWorld(const Eigen::Vector2d& position_center_wrt_world, double attitude_center_wrt_world);
    int getPoseCenterWrtWorld(Eigen::Vector2d& position_center_wrt_world, double& attitude_center_wrt_world) const;
    Eigen::Vector2d getPositionCenterWrtWorld() const;
    double getAttitudeCenterWrtWorld() const;

};




/////////////////////////////////
/// \brief The Rectangle2d class
/////////////////////////////////
class Rectangle2d : public Obstacle2d
{
public:
    Rectangle2d();
    ~Rectangle2d();
protected:
    int init();

protected:
    Eigen::Vector2d size_;
    bool flag_size_;
public:
    void setSize(const Eigen::Vector2d& size);
    int getSize(Eigen::Vector2d& size) const;
public:
    void setSizeI(int i, double size_i);
    int getSizeI(int i, double &size_i) const;

protected:
    std::vector<Eigen::Vector2d> vertices_;
    bool flag_vertices_;
public:
    void setVertices(const std::vector<Eigen::Vector2d>& vertices);
    int getVertices(std::vector<Eigen::Vector2d>& vertices) const;

private:
    std::vector<Eigen::Vector2i> edges_;
    bool flag_edges_;
public:
    void setEdges(const std::vector<Eigen::Vector2i>& edges);
    int getEdges(std::vector<Eigen::Vector2i>& edges) const;

public:
    int getArea(double& area) const;

private:
    int computePositionCenterFromVertices();
private:
    int computeAttitudeCenterFromVertices();

public:
    int computePoseCenterFromVertices();

public:
    int copyFrom(const Rectangle2d& rectangle);
    int copyTo(Rectangle2d& rectangle) const;

};



////////////////////////////////
/// \brief The Ellipse3d class
////////////////////////////////
class Ellipse2d : public Obstacle2d
{
public:
    Ellipse2d();
    ~Ellipse2d();
protected:
    int init();

protected:
    Eigen::Vector2d radii_;
    bool flag_radii_;
public:
    void setRadii(const Eigen::Vector2d& radii);
    Eigen::Vector2d getRadii() const;
};






//////////////////////////////////////
/// \brief The Obstacle2dConverter class
//////////////////////////////////////
class Obstacle2dConverter
{
public:
    static int convert(const Obstacle3d* obstacle3d, const Plane3d& plane3d, Obstacle2d*& obstacle2d);

protected:
    // TODO FIX bug with angle computation due to bad vertex. Normally appear for rectangles with angles not 0, 90 or 180
    static int convert(const Cuboid3d* cuboid3d, const Plane3d& plane3d, Rectangle2d*& rectangle2d);
protected:
    static int convert(const Cylinder3d* cylinder3d, const Plane3d& plane3d, Obstacle2d*& obstacle2d);


};




#endif
